package com.rezza.register.process;

import com.rezza.register.controller.RequestModel;
import com.rezza.register.db.mapper.TbMemberMapper;
import com.rezza.register.db.model.TbMember;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RegisterProcess {
    @Autowired
    private TbMemberMapper memberMapper;

    public int register(RequestModel requestModel) {
        TbMember member = new TbMember();
        member.setTbmMobilePhone(requestModel.getPhone());
        member.setTbmEmail(requestModel.getEmail());
        return memberMapper.register(member);
    }
}
package com.rezza.register.controller;

import com.rezza.register.process.RegisterProcess;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
@RequestMapping("/v1")
public class RegisterController {

    @Autowired
    private RegisterProcess registerProcess;

    @ApiOperation(value = "Register user")
    @PostMapping(value = "/reg", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity register(@RequestBody RequestModel payloads) {
        HashMap map = new HashMap();
        map.put("status", "failed");
        if (registerProcess.register(payloads) > 0)
            map.put("status", "success");
        return new ResponseEntity(map ,HttpStatus.OK);
    }
}
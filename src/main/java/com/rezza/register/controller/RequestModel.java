package com.rezza.register.controller;

import lombok.Data;

@Data
public class RequestModel {
    private String phone;
    private String email;
}
